var check_login_form = function(){
	var elements = document.login_form;
	var valid = true;

	if(!elements.username.value || !elements.password.value){
		document.getElementsByClassName("errors")[0].innerHTML = "<div class='error'>All fields must be filled</div>";
		valid = false;
	}

	return valid;
}

var check_singup_form = function(){
	var elements = document.singup_form;
	var valid = true;

	if(!elements.firstname.value || !elements.lastname.value || !elements.username.value || !elements.password.value){
		document.getElementsByClassName("errors")[0].innerHTML = "<div class='error'>All fields must be filled</div>";
		valid = false;
	} else {
		var fvalid = check_firstname();
		var lvalid = check_lastname();
		var uvalid = check_username();
		var pvalid = check_password();
		valid = (fvalid && lvalid && uvalid && pvalid);
	}

	return valid;
}

var check_firstname = function(){
	var valid = true;
	var value = document.singup_form.firstname.value;
	
	if(value.length < 2){
		document.getElementsByName("firstname_error")[0].innerHTML = "Too short firstname";
		valid = false;
	} else {
		document.getElementsByName("firstname_error")[0].innerHTML = "";
	}

	return valid;  
}

var check_lastname = function(){
	var valid = true;
	var value = document.singup_form.lastname.value;
	
	if(value.length < 2){
		document.getElementsByName("lastname_error")[0].innerHTML = "Too short lastname";
		valid = false;
	} else {
		document.getElementsByName("lastname_error")[0].innerHTML = "";
	}

	return valid;  
}

var check_username = function(){
	var valid = true;
	var value = document.singup_form.username.value;

	if(value.length < 4){
		document.getElementsByName("available")[0].innerHTML = "Too short username";
		valid = false;
	}  else {
		document.getElementsByName("available")[0].innerHTML = "";

		var params = "user=" + document.singup_form.username.value;	
		var req = getXmlHttp();

		req.onreadystatechange = function(){
			if(req.readyState == 4){
				if(req.status == 200){
					document.getElementsByName("available")[0].innerHTML = req.responseText;
				}
			}
		}

		req.open('POST', 'ajax/checkuser.php', true);
		req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		req.setRequestHeader("Content-length", params.length);
		req.setRequestHeader("Connection", "close");
		req.send(params);
	}

	return valid;
}

var check_password = function(){
	var valid = true;
	var value = document.singup_form.password.value;
	
	if(value.length < 4){
		document.getElementsByName("password_error")[0].innerHTML = "Too short password";
		valid = false;
	} else {
		document.getElementsByName("password_error")[0].innerHTML = "";
	}

	return valid;  
}

var getXmlHttp = function(){
	var xmlhttp;
	
	try{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e){
		try {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	    } catch (E){
			xmlhttp = false;
		}
	}

	if(!xmlhttp && typeof XMLHttpRequest != 'undefined'){
		xmlhttp = new XMLHttpRequest();
	}

	return xmlhttp;
}

var check_profileform = function(){

}

var image_select = function(){
	var mw = document.getElementById("modal_window");
	var b = document.getElementById("blind");

	open_modal(mw, b);

}

var open_modal = function(modal, blind){
	modal.style.display = "block";  
	blind.style.display = "block";  	
}

var close_modal = function(modal, blind){
	modal.style.display = "none";  
	blind.style.display = "none";  
}