<?php
include_once("markdown.php");
session_start();

$mysqli_host = "localhost";
$mysqli_user = "root"; // Place your mysql username here
$mysqli_password = "heart4Jesus"; // Place your mysql password here
$mysqli_db = "sn"; // Name of the database
$connection = mysqli_connect($mysqli_host, $mysqli_user, $mysqli_password, $mysqli_db);
$logged = FALSE;

if(isset($_SESSION['username']) && isset($_SESSION['password'])){
	$logged = TRUE;
}

if(mysqli_connect_errno()){
	die(mysqli_connect_error());
}

function check_string($string){
	return addslashes($string);
}

function markdown($text){
	return Parsedown::instance()->text($text);
}

function draw_avatar($username){
	if(file_exists("images/uploads/profiles/" . $username . ".png")){
		echo "<img src='images/uploads/profiles/" . $username . ".png' class='profile_i' aling='left'/>";
	} else {
		echo "<img src='images/assets/profile.jpeg' class='profile_i'/>";
	}
}

function draw_small_avatar($username){
	if(file_exists("images/uploads/profiles/" . $username)){
		echo "<img src='images/uploads/profiles/" . $username . "' class='profile_i_s' aling='left'/>";
	} else {
		echo "<img src='images/assets/profile.jpeg' class='profile_i_s'/>";
	}
}

function replace_tags($string){
	return str_replace(
		array(
			'<3', 
			':)', 
			':(', 
			'[heart]', 
			'[smile]', 
			'[disappointed]', 
			'[computer]', 
			'[gun]', 
			'[dollar]', 
			'[gift]', 
			'[guitar]', 
			'[email]', 
			'[cool]', 
			'[iceream]', 
			'[id]', 
			'[iphone]', 
			'[key]', 
			'[octocat]', 
			'[clock]', 
			'[coffee]', 
			'[fire]', 
			'[new]', 
			'[phone]', 
			'[helicopter]', 
			'[+1]', 
			'[-1]',
			'[hamburger]',
			'[sos]',
			'[spruce]',
			'[diamond]',
			'[bomb]',
			'[house]',
			'[joystick]',
			'[pizza]',
			'[recycle]',
			'[apple]',
			'[sun]',
			'[boom]',
			'[sleeping]',
			'[plane]',
			'[clouds]'
		), 
		array(
			'<img src=\'images/assets/emojis/heart.png\' class=\'emoji\'></img>', 
			'<img src=\'images/assets/emojis/smile.png\' class=\'emoji\'></img>', 
			'<img src=\'images/assets/emojis/disappointed.png\' class=\'emoji\'></img>', 
			'<img src=\'images/assets/emojis/heart.png\' class=\'emoji\'></img>', 
			'<img src=\'images/assets/emojis/smile.png\' class=\'emoji\'></img>', 
			'<img src=\'images/assets/emojis/disappointed.png\' class=\'emoji\'></img>', 
			'<img src=\'images/assets/emojis/computer.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/gun.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/dollar.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/gift.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/guitar.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/email.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/cool.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/icecream.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/id.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/iphone.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/key.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/octocat.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/clock.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/coffee.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/fire.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/new.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/phone.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/helicopter.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/+1.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/-1.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/hamburger.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/sos.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/spruce.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/diamond.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/bomb.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/house.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/joystick.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/pizza.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/recycle.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/pizza.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/apple.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/sun.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/boom.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/sleeping.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/plane.png\' class=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/clouds.png\' class=\'emoji\'></img>'
		), $string
	);
}
?>
