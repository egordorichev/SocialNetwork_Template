# SocialNetwork Template

<a href='https://raw.githubusercontent.com/egordorichev/SocialNetwork_Template/master/screenshot.png' title='Screenshot'><i>Screenshot</i></a>

SocialNetwork Template is a simple php based social network made for studing.

### Install

Copy all files to your host. Run `createdatabase.sql` with mysql.
In file `core/config.php` change `$mysql_user` and `$mysql_password` variables to your mysql uername and password.

### Root (Super user)

User with id 1 has full right on this site. He can edit and delete posts, edit profiles, drop friends. Be careful! :smile:  

#### Query

Root has access to `?query` page. Here he can run MySQL queries from here.

### Markdown

Markdown is supported. You can use it in your posts or profile.

### Emjis

Also here is some emojis. You can put them using square brackets and the keyword:

`[smile]` or `:)` = <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/smile.png' width='20px' height='20px'></img>

There is the full emojis list:

<table>
  <tr>
    <td>
    <3 or [heart]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/heart.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    :) or [smile]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/smile.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    :( or [disappointed]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/disappointed.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [computer]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/computer.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [gun]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/gun.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [dollar]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/dollar.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [gift]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/gift.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [guitar]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/guitar.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [email]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/email.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [cool]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/cool.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [icecream]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/icecream.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [id]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/id.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [iphone]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/iphone.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [key]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/key.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [octocat]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/octocat.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [clock]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/clock.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [coffee]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/coffee.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [fire]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/fire.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [ew]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/new.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [phone]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/phone.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [helicopter]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/helicopter.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [+1]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/+1.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [-1]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/-1.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [hamburger]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/hamburger.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [sos]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/sos.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [spruce]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/spruce.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [diamond]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/diamond.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [bomb]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/bomb.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [house]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/house.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [joystick]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/joystick.png' width='20px' height='20px'></img>
    </td>
  </tr>  
  <tr>
    <td>
    [pizza]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/pizza.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [recycle]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/recycle.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [apple]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/apple.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [sun]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/sun.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [boom]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/boom.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [sleeping]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/sleeping.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [plane]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/plane.png' width='20px' height='20px'></img>
    </td>
  </tr>
  <tr>
    <td>
    [clouds]
    </td>
    <td>
      <img src='https://github.com/egordorichev/SocialNetwork_Template/raw/master/images/assets/emojis/clouds.png' width='20px' height='20px'></img>
    </td>
  </tr>
</table>
