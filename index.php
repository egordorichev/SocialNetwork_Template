<?php
include_once("core/config.php");

echo <<<END
<!DOCTYPE html>
<html>
<head>
	<title>Social Network</title>
	<link rel="stylesheet" href="style.css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	<script src='http://code.jquery.com/jquery-2.1.4.min.js' type='text/javascript'></script>
	<script src='js/in.js' type='text/javascript'></script>
	<script src='http://cdnjs.cloudflare.com/ajax/libs/autosize.js/1.18.4/jquery.autosize.min.js' type='text/javascript'></script>
</head>
<body>
<script>
$(document).ready(function(){
	$('textarea').autosize();
});
</script>
END;

if($logged == FALSE){
	if(isset($_GET['singup'])){
echo <<<END
<form onsubmit='return check_singup_form();' method='post' autocomplete='off' name='singup_form' class='singup_form'>
	<input placeholder='Firstname' oninput='check_firstname()' class='firstname' name='firstname'></input><div class='firstname_error' name='firstname_error'></div>
	<input placeholder='Lastname' oninput='check_lastname()' class='lastname' name='lastname'></input><div class='lastname_error' name='lastname_error'></div>
	<input placeholder='Username' oninput='check_username()' class='username' name='username'></input><div class='available' name='available'></div>
	<input placeholder='Password' oninput='check_password()' type='password' class='password' name='password'></input><div class='password_error' name='password_error'></div>
	<div class='errors'></div>
	<a href='?singin' class='login_a'>Login</a>
	<button class='submit' type='submit'>Singup</button>
END;

		$error = "";
		$firstname = "";
		$lastname = "";
		$username = "";
		$password = "";
	
		if(isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['username']) && isset($_POST['password'])){
			$firstname = check_string($_POST['firstname']);
			$lastname = check_string($_POST['lastname']);
			$username = check_string($_POST['username']);
			$password = check_string($_POST['password']);
	
			if($firstname == "" || $lastname == "" || $username == "" || $password == ""){
				$error = "All fields must be filled";
			} else {
				if(mysqli_num_rows(mysqli_query($connection, "SELECT * FROM users WHERE username='$username'"))){
					$error = "This username is allready taken";
				} else {
					$password_hash = password_hash($password, PASSWORD_BCRYPT);
					$result = mysqli_fetch_row(mysqli_query($connection, "SELECT MAX(id) AS id FROM users"));					
					$id = $result[0];
					$id += 1;
	
					mysqli_query($connection, "INSERT INTO users VALUES('$id', '$username', '$password_hash', '$firstname', '$lastname', '')");
	
					$_SESSION['username'] = $username;
					$_SESSION['password'] = $password;
					$_SESSION['id'] = $id;
	
					$logged = TRUE;
	
					header("Location: ./");
				}
			}
		}
		
		if($error != ""){
			echo "<div class='error'>$error</div></form>";
		}
	} else {
echo <<<END
<form onsubmit='return check_login_form();' method='post' name='login_form' class='login_form'>
	<input placeholder='Username' class='username' name='username'></input><br/>
	<input placeholder='Password' type='password' class='password' name='password'></input><br/>
	<div class='errors'></div>
END;

		$error = "";
		$username = "";
		$password = "";
	
		if(isset($_POST['username']) && isset($_POST['password'])){
			$username = check_string($_POST['username']);
			$password = check_string($_POST['password']);
			
	
			if($username == "" || $password == ""){
				$error = "All fields must be filled";
			} else {
				$row = mysqli_fetch_row(mysqli_query($connection, "SELECT * FROM users WHERE username='$username'"));				
	
				if(password_verify($password, $row[2])){
					$_SESSION['id'] = $row[0];
					$_SESSION['username'] = $username;
					$_SESSION['password'] = $password;
					
					$logged = TRUE;
	
					header("Location: ./");
				} else {
					$error = "Wrong username or password";
				}
			}
		}
			
		if($error != ""){
			echo "<div class='error'>" . $error . "</div>";
		}
	
echo <<<END
	<a href='?singup' class='singup_a'>Singup</a>
	<button type='submit' class='submit'>Login</button>
</form>
END;
	}
} else {

echo <<<END
<div class='header'>
	<div class='h_c'>
	<a href='./' class='nav_a'>SocialNetwork</a>  
	<a href='?friends' class='nav_a'>Frieds</a>  
	<a href='?members' class='nav_a'>Members</a>  
	<a href='?logout' class='nav_a logout_a'></a>
END;

echo "<a href='?settings' class='nav_a settings_a'></a><a href='?profile' class='nav_a' style='float: right'>" . $_SESSION['username'] . "</a>";
echo <<<END
</div>
</div>
<div class='content'>

END;

	if(isset($_GET['logout'])){
		session_destroy();
		header("Location: ./");
	} else if(isset($_GET['settings'])){
		echo "TODO";
	} else if(isset($_GET['query']) && $_SESSION['id'] == 1){
echo <<<END
<form autocomplete='off' method='post' class='edit_profile_text'>
	<textarea name='q_text' class='profile_ta' placeholder='MySQL query ...'></textarea><br/>
	<button type='submit' style='margin-top: 5px; float: right;' class='submit'>Run</button>
</form>
END;

		if(isset($_POST['q_text'])){
			$result = mysqli_query($connection, $_POST['q_text']);

			echo "<br/><br/><table>";			 
			
			while($row = mysqli_fetch_assoc($result)){
				echo "<tr>";
				
				foreach($row as $cvalue){
					echo "<td>$cvalue</td>";
				}

				echo "</tr>";
			}

			echo "</table>";
			
			echo mysqli_error($connection);
			
		}
	} else if(isset($_GET['profile'])){
		echo "<b>Your profile</b> <a href='?editprofile' class='edit_profile'>edit</a><br/><br/>";

		$profile = mysqli_fetch_row(mysqli_query($connection, "SELECT * FROM users WHERE username='" . $_SESSION['username'] . "'"));				
		$img = glob("images/uploads/profiles/" . $_SESSION['username'] . "*");

		if(count($img > 0)){
			echo "<img src='$img[0]' class='profile_i' aling='left'/>";
		} else {
			echo "<img src='images/assets/profile.jpeg' class='profile_i' aling='left'/>";
		}

		echo "<h3>" . $profile[3] . " " . $profile[4] . "</h3>";
		echo "<div>" . markdown(replace_tags($profile[5])) . "</div><br/>";

		$messages = mysqli_query($connection, "SELECT * FROM messages WHERE userid='"  . $profile[0] . "' ORDER BY date DESC");
		$num = mysqli_num_rows($messages);

		for($i = 0; $i < $num; $i++){
			$row = mysqli_fetch_row($messages); 
			$user = mysqli_fetch_row(mysqli_query($connection, "SELECT * FROM users WHERE id='" . $row[2] . "'"));
			$likes = mysqli_num_rows(mysqli_query($connection, "SELECT * FROM likes WHERE postid='" . $row[0] . "'")); 
			$like_t = "Likes";
			$liked = "Like";
		
			if(mysqli_num_rows(mysqli_query($connection, "SELECT * FROM likes WHERE userid='" . $_SESSION['id'] . "' AND postid='" . $row[0] . "'"))){
				$liked = "Unlike";
			}
		
			if($likes == 1){
				$like_t = "Like";
			}
		
			draw_small_avatar($user[1]);
			echo "<div class='post'><div class='post_header'>";
			echo "<b><a class='profile_a' href='?view=" . $user[0] . "'>" . $user[1] . "</a></b> - " . date("M jS y g:ia", $row[1]);
		
			if($_SESSION['id'] == $row[2] || $_SESSION['id'] == 1){
				echo "<a class='erase' href='?erase=" . $row[0] . "'></a>";
				echo "<a class='edit' href='?editpost=" . $row[0] . "'></a>";
			}
		
			echo "</div><pre>";
			echo markdown(replace_tags($row[3]));
			echo "</pre><div class='post_footer'>";
						
			if($_SESSION['id'] != $row[2]){
				echo "<a href='?like=$row[0]' class='like_a'>$liked</a> · <a href='#comment=$row[0]' class='comm_a'>Comment</a>";
			}
		
			echo "<span class='likes'>$likes $like_t</span>";
		
			if($_SESSION['id'] != $row[2]){
				echo "<form method='post' class='add_comment'><textarea class='comment' placeholder='Comment ...'></textarea></form>";
			}
		
			echo "<div style='clear: both;'></div></div></div><br/>";
		}

		if(!$num){
			echo "There is no messages yet. <br/>";
		}
	} else if(isset($_GET['view'])){
		$id = check_string($_GET['view']);
		$profile = mysqli_fetch_row(mysqli_query($connection, "SELECT * FROM users WHERE id='$id'"));				
		
		draw_avatar($profile[1]);
		echo "<br/><b>" . $profile[3] . " " . $profile[4] . "</b>";

		if($_SESSION['id'] == 1 || $_SESSION['id'] == $id){
			echo "<a href='?editprofile=$id' class='edit_profile'> edit</a>";
		}

		echo "<div>" . markdown(replace_tags($profile[5])) . "</div><br/><div class='friends'><ul>";

		$friends = mysqli_query($connection, "SELECT * FROM friends WHERE userid='$id' OR friendid='$id'");
		$num = mysqli_num_rows($friends);

		for($i = 0; $i < $num; $i++){
			$row = mysqli_fetch_row($friends); 

			$friend = mysqli_fetch_row(mysqli_query($connection, "SELECT * FROM users WHERE id='" . $row[1] . "'"));

			echo "<li><a href='?view=" . $friend[0] . "' class='profile_a'>" . $friend[1] . "</a>";

			if($_SESSION['id'] == $id || $_SESSION['id'] == 1){
				echo "<a href='?dropfriend=" . $friend[0] . "'>[drop]</a></li>";
			}

			echo "</li>";
		}

		echo "</ul></div>";

		$messages = mysqli_query($connection, "SELECT * FROM messages WHERE userid='$id' ORDER BY date DESC");
		$num = mysqli_num_rows($messages);

		for($i = 0; $i < $num; $i++){
			$row = mysqli_fetch_row($messages); 
			$user = mysqli_fetch_row(mysqli_query($connection, "SELECT * FROM users WHERE id='" . $row[2] . "'"));
			$likes = mysqli_num_rows(mysqli_query($connection, "SELECT * FROM likes WHERE postid='" . $row[0] . "'")); 
			$like_t = "Likes";
			$liked = "Like";
		
			if(mysqli_num_rows(mysqli_query($connection, "SELECT * FROM likes WHERE userid='" . $_SESSION['id'] . "' AND postid='" . $row[0] . "'"))){
				$liked = "Unlike";
			}
		
			if($likes == 1){
				$like_t = "Like";
			}
		
			draw_small_avatar($user[1]);
			echo "<div class='post'><div class='post_header'>";
			echo "<b><a class='profile_a' href='?view=" . $user[0] . "'>" . $user[1] . "</a></b> - " . date("M jS y g:ia", $row[1]);
		
			if($_SESSION['id'] == $row[2] || $_SESSION['id'] == 1){
				echo "<a class='erase' href='?erase=" . $row[0] . "'></a>";
				echo "<a class='edit' href='?editpost=" . $row[0] . "'></a>";
			}
		
			echo "</div><pre>";
			echo markdown(replace_tags($row[3]));
			echo "</pre><div class='post_footer'>";
						
			if($_SESSION['id'] != $row[2]){
				echo "<a href='?like=$row[0]' class='like_a'>$liked</a> · <a href='#comment=$row[0]' class='comm_a'>Comment</a>";
			}
		
			echo "<span class='likes'>$likes $like_t</span>";
		
			if($_SESSION['id'] != $row[2]){
				echo "<form method='post' class='add_comment'><textarea class='comment' placeholder='Comment ...'></textarea></form>";
			}
		
			echo "<div style='clear: both;'></div></div></div><br/>";
		}

		if(!$num){
			echo "There is no messages yet. <br/>";
		}
	} else if(isset($_GET['editprofile'])){
		$p = check_string($_GET['editprofile']);

		if($p == 0){
			$profile = mysqli_fetch_row(mysqli_query($connection, "SELECT * FROM users WHERE username='" . $_SESSION['username'] . "'"));

echo <<<END

<form autocomplete='off' method='post' class='edit_profile_text' enctype='multipart/form-data'>
<textarea name='p_text' class='profile_ta'>
END;
				
			echo $profile[5];
echo <<<END
</textarea><br/>
	<input class="cropit-image-input" type='file' name='image' id='img_select' onchange='image_select();'>
	<button type='submit' style='margin-top: 5px; float: right;' class='submit'>Save</button>
</form>
<div id='modal_window'>
	<a onclick='close_modal(document.getElementById("modal_window"), document.getElementById("blind"))' class='close_modal'>Close X</a>	
</div>
<div id='blind'></div>
<br/>
END;

			if(isset($_POST['p_text'])){
				$text = check_string($_POST['p_text']);
				
				mysqli_query($connection, "UPDATE users SET text='$text' WHERE id='$p'");			
			}

			if(isset($_FILES['image']['tmp_name'])){
				$uploadOk = 1;
				$path = "images/uploads/profiles/" . $profile[1] . image_type_to_extension($check[2]);
						
				$uploadOk = 1;

				if($_FILES["image"]["size"] > 500000){
				   	$uploadOk = 0;
				}

				if($uploadOk){
					unlink("images/uploads/profiles/" . $profile[1]);
					move_uploaded_file($_FILES["image"]["tmp_name"], $path);
				}

				header("Location: ./?profile");
			}
		} else {
			if($_SESSION['id'] == 1 || $_SESSION['id'] == $p){
				$profile = mysqli_fetch_row(mysqli_query($connection, "SELECT * FROM users WHERE id='$p'"));

echo <<<END

<form autocomplete='off' method='post' class='edit_profile_text' enctype='multipart/form-data'>
<textarea name='p_text' class='profile_ta'>
END;
				
				echo $profile[5];
echo <<<END
</textarea><br/>
	<input type='file' name='image' size='14'>
	<button type='submit'>Save</button>
</form>
END;

				if(isset($_POST['p_text'])){
					$text = check_string($_POST['p_text']);
					mysqli_query($connection, "UPDATE users SET text='$text' WHERE id='$p'");		

					if(!isset($_FILES['image']['tmp_name'])){
						//header("Location: ./?view=" . $profile[0]);
					}	
				}

				if(isset($_FILES['image']['tmp_name'])){
					$uploadOk = 1;
					$check = getimagesize($_FILES["image"]["tmp_name"]);
					$path = "images/uploads/profiles/" . $profile[1] . image_type_to_extension($check[2]);
							
					$uploadOk = 1;
						
					if($uploadOk){
						unlink("images/uploads/profiles/" . $profile[1] . ".jpeg");
						unlink("images/uploads/profiles/" . $profile[1] . ".jpg");
						unlink("images/uploads/profiles/" . $profile[1] . ".png");
						unlink("images/uploads/profiles/" . $profile[1] . ".gif");
						move_uploaded_file($_FILES["image"]["tmp_name"], $path);
					}

					header("Location: ./?view=" . $profile[0]);
				}
			} else {
				echo "<h3>You do not have rights to edit this profile</h3>";
			}
		}
	} else if(isset($_GET['erase'])){
		$erase = check_string($_GET['erase']);
		$message = mysqli_fetch_row(mysqli_query($connection, "SELECT * FROM messages WHERE id='$erase'"));
		$owner = mysqli_fetch_row(mysqli_query($connection, "SELECT * FROM users WHERE id='" . $message[2] . "'"));

		if($_SESSION['id'] == 1 || $_SESSION['id'] == $owner[0]){
			mysqli_query($connection, "DELETE FROM messages WHERE id='$erase'");
			mysqli_query($connection, "DELETE FROM likes WHERE postid='$erase'");
			header("Location: ./");
		} else {
			echo "<h3>You do not have rights to delete this post</h3>";
		}
	} else if(isset($_GET['editpost'])){
		$text = "";
		$title = "";
		$edit = check_string($_GET['editpost']);
		$message = mysqli_fetch_row(mysqli_query($connection, "SELECT * FROM messages WHERE id='$edit'"));
		$owner = mysqli_fetch_row(mysqli_query($connection, "SELECT * FROM users WHERE id='" . $message[2] . "'"));

		if($_SESSION['id'] == 1 || $_SESSION['id'] == $owner[0]){
			if(isset($_POST['edit_ta'])){
				$text = check_string($_POST['edit_ta']);				

				if($text == ""){
					$error = "Please, add message";
				} else {
					mysqli_query($connection, "UPDATE messages SET text='$text' WHERE id='$edit'");
					header("Location: ./");
				}
			}
			
			$message = mysqli_fetch_row(mysqli_query($connection, "SELECT * FROM messages WHERE id='$edit'"));

			echo "<form autocomplete='off' method='post' class='edit_message'>";
			echo "	<textarea name='edit_ta' class='edit_ta' placeholder='Message'>" . $message['3'] . "</textarea><br/>";
			echo "	<button type='submit' class='save'>Save</button>";
			echo "<br/>";

			if($error != ""){
				echo "<div class='error'>$error</div>";
			}

			echo "</form>";
		} else {
			echo "<h3>You do not have rights to edit this post</h3>";
		}
	} else if(isset($_GET['friends'])){
		echo "<b>You friends</b><br/><ul>";

		$friends = mysqli_query($connection, "SELECT * FROM friends WHERE userid='" . $_SESSION['id'] . "'");
		$num = mysqli_num_rows($friends);

		for($i = 0; $i < $num; $i++){
			$row = mysqli_fetch_row($friends);
			$friend = mysqli_fetch_row(mysqli_query($connection, "SELECT * FROM users WHERE id='" . $row[1] . "'"));

			echo "<li><a href='?view=" . $friend[0] . "' class='profile_a'>" . $friend[1] . "</a> <a href='?dropfriend=" . $friend[0] . "'>[drop]</a></li>";
		}

		if(!$num){
			echo "You haven't got any friends";
		}

		echo "</ul>";
	} else if(isset($_GET['dropfriend'])){
		$id = check_string($_GET['dropfriend']);
		mysqli_query($connection, "DELETE FROM friends WHERE friendid='$id' AND userid='" . $_SESSION['id'] . "'");
		header("Location: ./?friends");
	} else if(isset($_GET['addfriend'])){
		$id = check_string($_GET['addfriend']);

		if(!mysqli_num_rows(mysqli_query($connection, "SELECT  * FROM friends WHERE friendid='$id' AND userid='" . $_SESSION['id'] . "'"))){
			mysqli_query($connection, "INSERT INTO friends VALUES('" . $_SESSION['id'] . "', '$id')");
		}

		header("Location: ./?friends");
	} else if(isset($_GET['members'])){
		echo "<b>Other members</b><br/><ul>";

		$members = mysqli_query($connection, "SELECT * FROM users");
		$num = mysqli_num_rows($members);

		for($i = 0; $i < $num; $i++){
			$row = mysqli_fetch_row($members);
				
			if($row[1] != $_SESSION['username']){
				$t1 = mysqli_num_rows(mysqli_query($connection, "SELECT * FROM friends WHERE userid='" . $_SESSION['id'] . "' AND friendid='" . $row[0] . "'"));
				$t2 = mysqli_num_rows(mysqli_query($connection, "SELECT * FROM friends WHERE friendid='" . $_SESSION['id'] . "' AND userid='" . $row[0] . "'"));
					
				echo "<li><a href='?view=" . $row['0'] . "' class='profile_a'>" . $row['1'] . "</a>";

				if(($t1 + $t2) > 1){
					echo " &harr; is your mutual friend";
					echo " <a href='?dropfriend=" . $row['0'] . "'>[drop]</a>";
				} else if($t1){
					echo " &larr; you are following";
					echo " <a href='?dropfriend=" . $row['0'] . "'>[drop]</a>";
				} else if($t2){
					echo " &rarr; is following you";
					echo " <a href='?addfriend=" . $row['0'] . "'>[follow]</a>";
				} else {
					echo " <a href='?addfriend=" . $row['0'] . "'>[follow]</a>";
				}

				echo "</li>";
			}	
		}

		echo "</ul>";

		if($num < 2){
			echo "There is no other members yet";
		}			
	} else if(isset($_GET['like'])){
		$like = check_string($_GET['like']);
		$liked = mysqli_num_rows(mysqli_query($connection, "SELECT * FROM likes WHERE userid='" . $_SESSION['id'] . "' AND postid='$like'"));

		if($liked){
			mysqli_query($connection, "DELETE FROM likes WHERE userid='" . $_SESSION['id'] . "' AND postid='$like'");
			header("Location: ./");
		} else {
			mysqli_query($connection, "INSERT INTO likes VALUES('" . $_SESSION['id'] . "', '$like')");
			header("Location: ./");
		}
	} else {

echo <<<END
<form autocomplete='off' method='post' class='add_message'>
	<textarea name='message_ta' class='message_ta' placeholder='Share something ...'></textarea>

END;

		if(isset($_POST['message_ta'])){
			$message = check_string($_POST['message_ta']);
			$userid = $_SESSION['id'];
			$time = time();
			$result = mysqli_fetch_assoc(mysqli_query($connection, "SELECT MAX(id) AS id FROM messages"));					
			$id = $result['id'];
			$id += 1;
			$error = "";
				
			if($message == ""){
				$error = "Please, add message";
			} else {
				mysqli_query($connection, "INSERT INTO messages VALUES('$id', '$time', '$userid', '$message')");
				header("Location: ./");
			}

			if($error != ""){
				echo "<div class='error'>$error</div>";
			}
		}

echo <<<END
	
	<button type='submit' class='save'>Post</button>
	<div style='clear:both;'></div>
</form>
<br/>
<br/>
END;

		$messages = mysqli_query($connection, "SELECT * FROM messages WHERE userid IN (SELECT friendid FROM friends WHERE userid='" . $_SESSION['id'] . "') OR userid='"  . $_SESSION['id'] . "' OR id IN (SELECT postid FROM likes WHERE userid IN (SELECT friendid FROM friends WHERE userid='" . $_SESSION['id'] . "')) ORDER BY date DESC");
		$num = mysqli_num_rows($messages);

		for($i = 0; $i < $num; $i++){
			$row = mysqli_fetch_row($messages); 
			$user = mysqli_fetch_row(mysqli_query($connection, "SELECT * FROM users WHERE id='" . $row[2] . "'"));
			$likes = mysqli_num_rows(mysqli_query($connection, "SELECT * FROM likes WHERE postid='" . $row[0] . "'")); 
			$like_t = "Likes";
			$liked = "Like";
		
			if(mysqli_num_rows(mysqli_query($connection, "SELECT * FROM likes WHERE userid='" . $_SESSION['id'] . "' AND postid='" . $row[0] . "'"))){
				$liked = "Unlike";
			}
		
			if($likes == 1){
				$like_t = "Like";
			}
		
			draw_small_avatar($user[1]);
			echo "<div class='post'><div class='post_header'>";
			echo "<b><a class='profile_a' href='?view=" . $user[0] . "'>" . $user[1] . "</a></b> - " . date("M jS y g:ia", $row[1]);
		
			if($_SESSION['id'] == $row[2] || $_SESSION['id'] == 1){
				echo "<a class='erase' href='?erase=" . $row[0] . "'></a>";
				echo "<a class='edit' href='?editpost=" . $row[0] . "'></a>";
			}
		
			echo "</div><pre>";
			echo markdown(replace_tags($row[3]));
			echo "</pre><div class='post_footer'>";
						
			if($_SESSION['id'] != $row[2]){
				echo "<a href='?like=$row[0]' class='like_a'>$liked</a> · <a href='#comment=$row[0]' class='comm_a'>Comment</a>";
			}
		
			echo "<span class='likes'>$likes $like_t</span>";
		
			if($_SESSION['id'] != $row[2]){
				echo "<form method='post' class='add_comment'><textarea class='comment' placeholder='Comment ...'></textarea></form>";
			}
		
			echo "<div style='clear: both;'></div></div></div><br/>";
		}

		if(!$num){
			echo "There is no messages yet. <br/>";
		}
	}

echo <<<END
</div>
</body>
END;
}

?>
